import React, { Component } from 'react';

class Display extends Component {

    constructor() {
        super();
        this.state = {
            numberOfUsers: null,
        };
    }

    async addUser() {
        // Simple PUT request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ title: 'React POST Request Example' })
        };
        await fetch('http://localhost:8080/user/add/' + ++this.state.numberOfUsers + '?email=example@email.com', requestOptions);
        this.componentDidMount();
    }

    componentDidMount() {
        // Simple GET request using fetch
        fetch('http://localhost:8080/counter/number')
            .then(response => response.json())
            .then(data => this.setState({ numberOfUsers: data.numberOfUsers }));
    }

    render() {
        const { numberOfUsers } = this.state;

        return (
            <div>
                <h1>Simple GET Request</h1>
                <p>Number Of Users: {numberOfUsers}</p>
                <button onClick={() => this.addUser()}>Add user</button>
            </div>
        );
    }
}

export default Display;
