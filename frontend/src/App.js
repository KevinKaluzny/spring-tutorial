import React, { Component } from 'react';
import './index.css';
import Display from "./component/display/Display";
import { BrowserRouter as Router } from "react-router-dom";

class App extends Component {
    render() {
        return (
            <div className="test">
                <Router>
                    <div>
                        <Display />
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;

/*
var express = require('express')
var cors = require('cors')
var app = express()

app.use(cors())

app.get('/user/number', function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
})
*/
