package com.example.demo.service;

import com.example.demo.domain.NumberOfUsersEntity;
import com.example.demo.domain.User;
import com.example.demo.repository.NumberOfUsersRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NumberOfUsersRepository numberOfUsersRepository;

    public void addUser(Long id, String email) {
        User user = new User(id, email);
        userRepository.save(user);

        NumberOfUsersEntity counter = numberOfUsersRepository.getById(1L);
        counter.setNumberOfUsers(counter.getNumberOfUsers() + 1);
        numberOfUsersRepository.save(counter);
    }

    public void deleteUser(Long id) {
        User user = new User(id);
        userRepository.delete(user);

        NumberOfUsersEntity counter = numberOfUsersRepository.getById(1L);
        counter.setNumberOfUsers(counter.getNumberOfUsers() - 1);
        numberOfUsersRepository.save(counter);
    }

    public void updateUser(Long id, String email) {
        Optional<User> user = userRepository.findById(id);
        user.get().saveEmail(email);
        Optional.ofNullable(userRepository.save(user.get()));
    }
}
