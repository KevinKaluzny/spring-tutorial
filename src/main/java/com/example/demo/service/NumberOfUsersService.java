package com.example.demo.service;

import com.example.demo.domain.NumberOfUsersEntity;
import com.example.demo.controller.NumberOfUsersDto;
import com.example.demo.repository.NumberOfUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NumberOfUsersService {

    @Autowired
    private NumberOfUsersRepository numberOfUsersRepository;

    public NumberOfUsersDto getNumberOfUsers() {
        NumberOfUsersEntity numberOfUsersEntity = numberOfUsersRepository.findById(1L).get();
        NumberOfUsersDto numberOfUsers = new NumberOfUsersDto(numberOfUsersEntity.getNumberOfUsers());
        return numberOfUsers;
    }

    public void addCounter() {
        NumberOfUsersEntity numberOfUsers = new NumberOfUsersEntity(1L);
        numberOfUsersRepository.save(numberOfUsers);
    }

    public void updateCounter(int number) {
        NumberOfUsersEntity numberOfUsers = numberOfUsersRepository.getById(1L);
        numberOfUsers.setNumberOfUsers(number);
        numberOfUsersRepository.save(numberOfUsers);
    }
}
