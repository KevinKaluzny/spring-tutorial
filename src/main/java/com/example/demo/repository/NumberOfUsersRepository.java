package com.example.demo.repository;

import com.example.demo.domain.NumberOfUsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NumberOfUsersRepository extends JpaRepository<NumberOfUsersEntity, Long> {

    @Override
    List<NumberOfUsersEntity> findAll();

    NumberOfUsersEntity getById(long l);

    @Override
    <S extends NumberOfUsersEntity> S save(S s);
}