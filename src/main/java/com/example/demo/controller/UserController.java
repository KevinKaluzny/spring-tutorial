package com.example.demo.controller;

import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/add/{id}", method = RequestMethod.POST)
    public void addUser(@PathVariable Long id, @RequestParam String email){
        userService.addUser(id, email);
    }

    @RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @RequestMapping(value="/update/{id}", method = RequestMethod.PUT)
    public void updateUser(@PathVariable Long id, @RequestParam String email) {
        userService.updateUser(id, email);
    }
}