package com.example.demo.controller;

public class NumberOfUsersDto {

    private int counter;

    public NumberOfUsersDto(int counter) {
        this.counter = counter;
    }

    // Getter
    public int getNumberOfUsers() {
        return counter;
    }

    // Setter
    public void setNumberOfUsers(int number) {
        counter = number;
    }
}
