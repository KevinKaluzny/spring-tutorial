package com.example.demo.controller;

import com.example.demo.service.NumberOfUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/counter")
public class NumberOfUsersController {

    @Autowired
    private NumberOfUsersService numberOfUsersService;

    @RequestMapping(value="/number", method = RequestMethod.GET)
    public NumberOfUsersDto getNumberOfUsers(){
        return numberOfUsersService.getNumberOfUsers();
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public void addCounter() {
        numberOfUsersService.addCounter();
    }

    @RequestMapping(value="/update", method = RequestMethod.PUT)
    public void setNumberOfUsers(@RequestParam int number) {
        numberOfUsersService.updateCounter(number);
    }
}
