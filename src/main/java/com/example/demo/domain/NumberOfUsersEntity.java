package com.example.demo.domain;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="NUMBER_OF_USERS")
public class NumberOfUsersEntity {

    @Id
    private Long id;

    @Column(name="COUNTER")
    private int counter;

    public NumberOfUsersEntity() {}

    public NumberOfUsersEntity(Long id) {
        this.id = id;
    }

    // Getter
    public int getNumberOfUsers() {
        return counter;
    }

    // Setter
    public void setNumberOfUsers(int number) {
        counter = number;
    }
}
