package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name="USERS")
public class User {
    public User() {}

    public User(Long id) {
        this.id = id;
    }

    public User(Long id, String email) {
        this.id = id;
        this.email = email;
    }

    public void saveEmail(String email) {
        this.email = email;
    }

    @Id
    private Long id;

    @Column(name = "EMAIL")
    private String email;
}
